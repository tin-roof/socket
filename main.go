package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"socket/packages/channel"
	"socket/packages/socket"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

var addr = flag.String("listen", ":9001", "addr to listen")

// Define variables used to know when to close the channel
var (
	closeInvalidPayload = ws.MustCompileFrame(
		ws.NewCloseFrame(ws.NewCloseFrameBody(
			ws.StatusInvalidFramePayloadData, "",
		)),
	)
	closeProtocolError = ws.MustCompileFrame(
		ws.NewCloseFrame(ws.NewCloseFrameBody(
			ws.StatusProtocolError, "",
		)),
	)
)

// handler Handle the request sent from the client
func handler(w http.ResponseWriter, r *http.Request) {

	// Upgrade the http request to tcp request
	conn, _, _, err := ws.UpgradeHTTP(r, w)
	if err != nil {
		log.Printf("upgrade error: %s", err)
		return
	}
	defer conn.Close()

	// DO WEBSOCKET MAGIC (I havnt had time to look at what htis is doing yet)
	state := ws.StateServerSide

	textPending := false
	utf8Reader := wsutil.NewUTF8Reader(nil)
	cipherReader := wsutil.NewCipherReader(nil, [4]byte{0, 0, 0, 0})

	for {
		header, err := ws.ReadHeader(conn)
		if err != nil {
			log.Printf("read header error: %s", err)
			break
		}
		if err = ws.CheckHeader(header, state); err != nil {
			log.Printf("header check error: %s", err)
			conn.Write(closeProtocolError)
			return
		}

		cipherReader.Reset(
			io.LimitReader(conn, header.Length),
			header.Mask,
		)

		var utf8Fin bool
		var r io.Reader = cipherReader

		switch header.OpCode {
		case ws.OpPing:
			header.OpCode = ws.OpPong
			header.Masked = false
			ws.WriteHeader(conn, header)
			io.CopyN(conn, cipherReader, header.Length)
			continue

		case ws.OpPong:
			io.CopyN(ioutil.Discard, conn, header.Length)
			continue

		case ws.OpClose:
			utf8Fin = true

		case ws.OpContinuation:
			if textPending {
				utf8Reader.Source = cipherReader
				r = utf8Reader
			}
			if header.Fin {
				state = state.Clear(ws.StateFragmented)
				textPending = false
				utf8Fin = true
			}

		case ws.OpText:
			utf8Reader.Reset(cipherReader)
			r = utf8Reader

			if !header.Fin {
				state = state.Set(ws.StateFragmented)
				textPending = true
			} else {
				utf8Fin = true
			}

		case ws.OpBinary:
			if !header.Fin {
				state = state.Set(ws.StateFragmented)
			}
		}

		payload := make([]byte, header.Length)
		_, err = io.ReadFull(r, payload)
		if err == nil && utf8Fin && !utf8Reader.Valid() {
			err = wsutil.ErrInvalidUTF8
		}
		if err != nil {
			log.Printf("read payload error: %s", err)
			if err == wsutil.ErrInvalidUTF8 {
				conn.Write(closeInvalidPayload)
			} else {
				conn.Write(ws.CompiledClose)
			}
			return
		}

		if header.OpCode == ws.OpClose {
			code, reason := ws.ParseCloseFrameData(payload)
			log.Printf("close frame received: %v %v", code, reason)

			if !code.Empty() {
				switch {
				case code.IsProtocolSpec() && !code.IsProtocolDefined():
					err = fmt.Errorf("close code from spec range is not defined")
				default:
					err = ws.CheckCloseFrameData(code, reason)
				}
				if err != nil {
					log.Printf("invalid close data: %s", err)
					conn.Write(closeProtocolError)
				} else {
					ws.WriteFrame(conn, ws.NewCloseFrame(ws.NewCloseFrameBody(
						code, "",
					)))
				}
				return
			}

			conn.Write(ws.CompiledClose)
			return
		}

		// Send the request data to the WS handler to break down the action that needs to happen
		go socket.Request(payload, conn, header)

		// Return the gerneric message received message
		header.Masked = false
		header.Length = 56
		ws.WriteHeader(conn, header)
		conn.Write([]byte(`{"msg":"Message received","status":"success","code":200}`))
	}
}

// main Handles the initial setup of the socket server and defines some global variables
func main() {

	// @TODO: load settings

	// Initialize the channel object
	err := channel.Init()
	if err != nil {
		// @TODO: log error and shut down the server
	}

	log.SetFlags(0)
	flag.Parse()

	// Only accept requests to the /ws route
	http.HandleFunc("/ws", handler)

	// Start listening over the tcp connections
	ln, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatalf("listen %q error: %v", *addr, err)
	}
	log.Printf("listening %s (%q)", ln.Addr(), *addr)

	// Create the webserver
	var server = new(http.Server)

	// Create the error handling channels
	var serve = make(chan error, 1)
	var sig = make(chan os.Signal, 1)

	signal.Notify(sig, syscall.SIGTERM)
	go func() { serve <- server.Serve(ln) }()

	select {
	// Log the error from the serve channel
	case err := <-serve:
		log.Fatal(err)

	// Shut down the server because a signal was sent
	case sig := <-sig:
		const timeout = 5 * time.Second

		// Log the signal for for debugging
		log.Printf("signal %q received; shutting down with %s timeout", sig, timeout)

		// Shut down the server after gracefully closing the processes
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		if err := server.Shutdown(ctx); err != nil {
			log.Fatal(err)
		}
		cancel()
	}
}
