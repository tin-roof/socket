package socket

import (
	"encoding/json"
	"fmt"
	"net"
	"strconv"

	"socket/packages/channel"

	"github.com/gobwas/ws"
)

// request from the client to the server
type request struct {
	Method string // The socket method that should be called
	Auth   struct {
		Key   string // API key used for the channel
		Token string // Users auth token for identification
	} // Settings used for identification in the socket server
	Data struct {
		Setting string
		Value   int
	} // @TODO: need to define the data that may go in here might need to be an interface so we can define the data based on the Method
}

// Request read a request and figure out what to do with it
func Request(payload []byte, conn net.Conn, header ws.Header) {

	// Define the request
	var req request

	// Unmarshal the payload
	err := json.Unmarshal(payload, &req)
	if err != nil {
		fmt.Println(err.Error())
	}

	// Handle the request
	switch req.Method {

	// Authorize / add a user to a channel
	case "authorize":
		fmt.Println("Adding a new user to a channel")

		// Add a user to the channel
		channel.AddUser(req.Auth.Key, req.Auth.Token, conn)

		header.Masked = false
		header.Length = 47
		ws.WriteHeader(conn, header)
		conn.Write([]byte(`{"msg":"success","status":"success","code":300}`))

		return

	// Close a users connection
	case "close":

		return

	// Update API key settings
	case "settings":
		// @TODO: Validate the users authorization
		fmt.Println("---Updating the Settings---")
		fmt.Println(string(payload))
		fmt.Println("---Finished Update---")

		count := req.Data.Value
		count++
		response := []byte(`{"msg":"success","status":"success","code":600,"setting":` + strconv.Itoa(count) + `}`)

		header.Masked = false
		header.Length = int64(len(response))

		// Loop through the channel and send the message to all users
		for _, usr := range channel.Channel.Map[req.Auth.Key] {
			ws.WriteHeader(usr.Connection, header)
			usr.Connection.Write(response)
		}

		return

	// Message the group
	case "message":

		return

	// Default error message
	default:
		fmt.Println("Method is not supported")

		header.Masked = false
		header.Length = 47
		ws.WriteHeader(conn, header)
		conn.Write([]byte(`{"msg":"success","status":"success","code":400}`))
	}
}
